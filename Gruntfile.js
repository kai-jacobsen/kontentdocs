module.exports = function(grunt) {

    require('time-grunt')(grunt);

  grunt.initConfig({
    mkdocs: {
      dist: {
        src: 'docs/',
        options: {
          clean: true
        }
      }
    },
    watch: {
      scripts: {
        files: ['docs/docs/**/*.md'],
        tasks: ['default'],
        options: {
          spawn: false
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-mkdocs');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.registerTask('default', ['watch']);
};
